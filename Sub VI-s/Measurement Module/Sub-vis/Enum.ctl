RSRC
 LVCCLBVW  �        �      Main_Class.lvclass    � �             < � @�      ����            t9��YN�ij�%{�          �ߌ�ծM�XV��g���ُ ��	���B~       �6�@n��K�x�zR�   ����   -7@�R
JN�-!�6_ɵ          H LVCC    VILB    PTH0   &      
Main_ClassMain_Class.lvclass             "   x�c`c`j`�� Č@������ �3  h6     H  x�c`��� H1200��,h�`Ʀ&�e..����P7�BD�bL{�&�T���) >�n�{j(    VIDS       q  �x�K`d`�4�0� ���X���!9?%���g��&�A T�����i@��/��7���vQh�Q�`*��Q��Qa�dQy������Gxw{�w���� �wQ� q�4���R3/ hGC�2C	�ၨ��L!Ì�B���'��h�@�5#����N� i�w=4�>����,ݍ@�މ (���!q܅CG�g<�	tN'��p���(Q�4��d2XM7�q�?D T���P j8��p�������v��9J��^����,��P�����b�� �@�HzX��d�@Ձ؇�l�]lP1^��	P��� eK��l% [ ���?@�zP����+r��7 >9q�       �$  15.0     �   15.0     �$  15.0     �   15.0     �$  15.0                       ������  �  �@ �� a�� ��  �� !�� A�� ��� �� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��              ��    ��      +����            �  �   ��   ��V                    �   ��       ���               �    ��       ���              �     ��       ���             �      ��       ���             ����   ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������          FPHP       �  �x����Kaǟw�m�w��.��.�N�������2�
����������A��A�����x�%�9Fy��ꥋ�L���ή����|?���g_ �=k�x&��|�{���P������o -	"`�N�- �yϦC����p;|O�+���C,����Ƽ�f^�>�;�B}D5��-�@�ƭ�tѽ����̒ ���u7u�����W#K
i`�����ǐ���*$IC������X]]��̲�Vm���b�H��&��|��^��)�Y��8V�B�.E��͎�puO��Q-�g�nkku+��Z�?Bt3�������M�g�7'�*�	�H6��Wd��,Յ�y��\8�Ћ.���,���GІ�l����φ��X2���v1�����h��Ts���㜥R�u�P�5��/F�}���ݩ`���~\Q�bA+4��@�#j1G�$����8^Fk
ݧf8T�����8���'��	꺋Vٻ8�t����,U��6MI�ߩ��˘�qo�aպ�+wS���.�g����w3��\�~즙��37q;2p����ڙ�E�m,�0���2#�6=����v2�S`nc�`l�I�Xnb��n���^0lY��:�>�1T��e+x���s��Н��S�*ݏ�����h�KZ�                  BDHP        b   rx�c``��`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-�����z�\�8Se�<� b           �      !_ni_LastKnownOwningLVClassCluster  � �      0����     �   z    $@@ ����  Triangle Start indexes  @T Demod Signal  @@ ���� Triangle part @ Chirp Number  @
 Chirp ID  @
 Chirp Start @
 	Chirp end @
 Chirp Duration  @
 Freq. start @
 	Freq. end @
 	Deviation Q@
 JRMS deviation of chirp frequency from an ideal linear frequency trajectory  Q@
 JMax deviation of chirp frequency from an ideal linear frequency trajectory  U@
 NAverage deviation of chirp frequency from an ideal linear frequency trajectory  C@
 =Average chirp frequency (with respect to center of the chirp) %@
 mean Deviation for this section @
 Chirp Rate (Hz/us)  @
 CW Freq.  g �       Main_Class.lvclassAnalyzed signal cluster.ctl 0@P       	 
        Cluster  @@ ���� Analyzed CW Signal  &@@ ���� Analyzed Rising Sawtooth  &@@ ���� Analyzed Falling Sawtooth @@ ����  CW End  @@ ����  CW Start  @P   	CW Period @@ ����  
Fall Start  @@ ����  Fall End   @P   Falling Chirp Period  @T Demod signal  @@ ���� Falling Part  @@ ����  Rise End  @@ ����  
Rise Start  @P    Rising Chirp Period @@ ���� Rising part @@ ���� Analyzed Triangler �       Main_Class.lvclassProcessing States.ctlA@ Config SpectogramConfig SpectrumErrorWrite Burst in TDMSGet All data lengthTDMS config.Start DemodulationReady For New RunStream. waitProcess. WaitConfig StreamStreamFind OFF periods
Find BurstMeasurementsSpectrum
SpectogramGet RAW data lengthGet Demod data length
TDMS close Processing States @
 RBW for Spectogram  @
 RBW of Spectrum @
 Spectogram End  @
 Spectogram Start  @
 Spectrum End  @
 Spectrum Start  @
 	Time Step @2����Data location S �       Main_Class.lvclassStream data duration.ctl @
 Stream data duration  
@!stop  @!status  @ code  @0����source  @P  / 0 1Error Cluster A �       Main_Class.lvclassStop to read.ctl @
 Stop o read F �       Main_Class.lvclassStart to read.ctl @
 Start for read  @p "	TDMS Path @
 Demod. signal IQ rate @
 RAW data IQ rate  @@ ����  OFF End @@ ����  	OFF Start @P  8 9
OFF Period  @T Demodulated Signal  @
 CW Deviation  @
 Numeric @@ ���� =Freq. start @@ ���� =	Freq. end @@ ���� =	Deviation  @@ ���� =Chirp Rate (Hz/us)  @@ ���� =Chirp Start @@ ���� =	Chirp end @@ ���� =Chirp Duration  @
 	rms value X@@ ���� EJRMS deviation of chirp frequency from an ideal linear frequency trajectory   
  X@@ ���� GJMax deviation of chirp frequency from an ideal linear frequency trajectory  @
 mean  \@@ ���� INAverage deviation of chirp frequency from an ideal linear frequency trajectory  J@@ ���� ==Average chirp frequency (with respect to center of the chirp) ,@@ ���� =mean Deviation for this section @ Numeric @@ ���� MChirp Number  @@ ���� =Chirp ID  @@ ���� =CW Freq.  0@P  > ? @ A B C D F H J K L N O P
Table data  @ Last chirp number  0���� $@@ �������� SMeasurement Table   @
 Carrier frequency @
 Power in band @
 Raw data power  @
 Span for power in band  @
 TDMS Read start @
 TDMS Read Stop  @
 IQ Rate @
 All aquired data length @
 Burst RAW data length @
 Demod. data length  @P  \ ] ^Data Lengths  @2����Stream Location @
 Stream Duration @
 Burst Start @
 
Burst Stop  @P  b cBurst Section @!Finish reading data 
@!Exit  @ FFT size  @
 center frequency  @ zoom  @
 trim  @P  h i jzoom settings @ window  @P  l gFFT settings  W �       NI_MAPro.lvlibma_sml_ND Zoom FFT Settings.ctl  @P  k mzoom FFT settings @
 frequency shift S@ realcomplex modulated (array)complex modulated (interleaved)  data format L �       niSMT.lvlibSMT zoom settings.ctl "@P  n o pSMT zoom settings @P  g qSpectrum Settings 	@
 f0  	@
 df  @@ ���� =spectrum  @P  s t uSpectrum  @
 TDMS Read dur.  @@ �������� =
Spectogram  �@P 3         ! " # $ % & ' ( ) * + , - . 2 3 4 5 6 7 : ; < Q R T U V W X Y Z [ _ ` a d e f r v w xMain_Class.lvclass   y                3   (                                        �  0x�m��j�P��ɍ��4�w}��V����`�"��0J�}d}�����Μs��p�'Ɯ(�����}�Ż]?;��;�n���U�ы^�mdC{�~�CE�Av�T�K�Pi=���� �u�e^�۔n��^�J��TB��$2/���|��٦`�݇F���z偖�m5��]p�\����FW��e�<*6� ���   e       H      � �   Q      � �   Z      � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW  �        �               4     LIBN      TLVSR      hRTSG      |CCST      �LIvi      �CONP      �TM80      �DFDS      �LIds      �VICD      vers     GCPR      �ICON      �icl8      �CPC2      �LIfp      �FPHb      �FPSE      �VPDP      LIbd       BDHb      4BDSE      HVITS      \DTHP      pMUID      �HIST      �VCTP      �FTAB      �    ����                                   ����       �        ����       �        ����       �        ����              ����               ����      H        ����      �        ����      �       ����             ����      ,       ����      <       	����      L       
����      \        ����      l        ����      �        ����              ����              ����              ����               ����      
�        ����      
�        ����      
�        ����      
�        ����      X        ����      `        ����      P        ����      X        ����      `        ����      �       �����      \    Enum.ctl