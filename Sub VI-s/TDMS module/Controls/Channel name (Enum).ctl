RSRC
 LVCCLBVW  8               Main_Class.lvclass    � �            < � @�      ����            n2rW�f�G��,E�~��          � �����B�����L"��ُ ��	���B~       5�)���M�$57�:�   ����   �i����Dbj��Z��          H LVCC    VILB    PTH0   &      
Main_ClassMain_Class.lvclass             "   x�c`c`j`�� Č@������ �3  h6     H  x�c`��� H1200��,h�`Ʀ&�e..����P7�BD�bL{�&�T���) >�n�{j(    VIDS       q  �x�K`d`�4�0� ���X���!9?%���g��&�A T�����i@��/��7���vQh�Q�`*��Q��Qa�dQy������Gxw{�w���� �wQ� q�4���R3/ hGC�2C	�ၨ��L!Ì�B���'��h�@�5#����N� i�w=4�>����,ݍ@�މ (���!q܅CG�g<�	tN'��p���(Q�4��d2XM7�q�?D T���P j8��p�������v��9J��^����,��P�����b�� �@�HzX��d�@Ձ؇�l�]lP1^��	P��� eK��l% [ ���?@�zP����+r��7 >9q�       �$  15.0     �   15.0     �$  15.0     �   15.0     �$  15.0                       ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������          FPHP       �  x��T�KQ��q��ۚoL�uv�7��k[�����"Q�X]b��Hp��]� ��A� �������.�b�2�b!�<lz
�������;�jP�<ff���}�����3�&����ͨ>�" ���O[ 6N~i.�q�)I�~��hT_�o�.픾�<�l��X �|64V�<���j����l��;\�A��Y,f���R��<3|#6�0����!鵁�VC�Π"�~#$�
Y��JJv���Z�$�I��q��dQv���4�V�/��Op�β[GW�/��0��n1�"��r֭�+�e����F��C�ya�������}Z*�> ��|�IA`�0���1����۰<��:��E
��z0rU���Y%�C�1���L&y(��6D�d ����l���_�>K��3��c�{{{eQ3Y�dr�7b���胊l����=�~�*Z�4�A���zG�أD<��$���`,�83�JA#���̤���[�Q��Y#3K����$�SI�$�s~΢�>^����h�ᶴ�����!\e�^bR��r�~�$�,\��8;�BWVV��A�W������n��N�w���ɣ��<�G��i�jj2�W7lT����3J���ջ�����3�j��-HZ�PI����w��q��E���f���L��bC��̆�J�;6�,?�Cl�+<�N�U��ۨ�T��*�|��<�ж߬4w                    BDHP        b   rx�c``��`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-�����z�\�8Se�<� b           �      !_ni_LastKnownOwningLVClassCluster  b �      0����     F   \    $@@ ����  Triangle Start indexes  @T Demod Signal  @@ ���� Triangle part @ Chirp Number  @
 Chirp ID  @
 Chirp Start @
 	Chirp end @
 Chirp Duration  @
 Freq. start @
 	Freq. end @
 	Deviation Q@
 JRMS deviation of chirp frequency from an ideal linear frequency trajectory  Q@
 JMax deviation of chirp frequency from an ideal linear frequency trajectory  U@
 NAverage deviation of chirp frequency from an ideal linear frequency trajectory  C@
 =Average chirp frequency (with respect to center of the chirp) %@
 mean Deviation for this section @
 Chirp Rate (Hz/us)  @
 CW Freq.  g �       Main_Class.lvclassAnalyzed signal cluster.ctl 0@P       	 
        Cluster  @@ ���� Analyzed CW Signal  &@@ ���� Analyzed Rising Sawtooth  &@@ ���� Analyzed Falling Sawtooth @@ ����  CW End  @@ ����  CW Start  @P   	CW Period @@ ����  
Fall Start  @@ ����  Fall End   @P   Falling Chirp Period  @T Demod signal  @@ ���� Falling Part  @@ ����  Rise End  @@ ����  
Rise Start  @P    Rising Chirp Period @@ ���� Rising part @@ ���� Analyzed Triangle> �       Main_Class.lvclassProcessing States.ctl@ TDMS config.Start DemodulationGet Demod. data IQ RateGet RAW data IQ RateReady For New RunStream. waitDemod. Read WaitDemod write. WaitProcess. WaitConfig StreamStreamFind OFF periods
Find BurstMeasurementsSpectrum
Spectogram  Processing States @
 RBW for Spectogram  @
 RBW of Spectrum @
 Spectogram End  @
 Spectogram Start  @
 Spectrum End  @
 Spectrum Start  @
 	Time Step @2����Data location S �       Main_Class.lvclassStream data duration.ctl @
 Stream data duration  
@!stop  @!status  @ code  @0����source  @P  / 0 1Error Cluster A �       Main_Class.lvclassStop to read.ctl @
 Stop o read F �       Main_Class.lvclassStart to read.ctl @
 Start for read  @p "	TDMS Path @
 Demod. signal IQ rate @
 RAW data IQ rate  @@ ����  OFF End @@ ����  	OFF Start @P  8 9
OFF Period  @T Demodulated Signal  @
 CW Deviation  @
 Numeric @@ ���� =Freq. start @@ ���� =	Freq. end @@ ���� =	Deviation  @@ ���� =Chirp Rate (Hz/us)  @@ ���� =Chirp Start @@ ���� =	Chirp end @@ ���� =Chirp Duration  @
 	rms value X@@ ���� EJRMS deviation of chirp frequency from an ideal linear frequency trajectory   
  X@@ ���� GJMax deviation of chirp frequency from an ideal linear frequency trajectory  @
 mean  \@@ ���� INAverage deviation of chirp frequency from an ideal linear frequency trajectory  J@@ ���� ==Average chirp frequency (with respect to center of the chirp) ,@@ ���� =mean Deviation for this section @ Numeric @@ ���� MChirp Number  @@ ���� =Chirp ID  @@ ���� =CW Freq.  0@P  > ? @ A B C D F H J K L N O P
Table data  @ Last chirp number  0���� $@@ �������� SMeasurement Table   @
 Carrier frequency @
 Power in band @
 Raw data power  @
 Span for power in band  @
 TDMS Read start @
 TDMS Read Stop  j@P (         ! " # $ % & ' ( ) * + , - . 2 3 4 5 6 7 : ; < Q R T U V W X Y ZMain_Class.lvclass   [       NI.LV.All.SourceOnly    �      !                  5   (                                         �x��Q=O�0}��6m��E���t��ؐ�2t` +v ��@b#F~�����?�|���>d �����oDi�WZ4�B���}�z�*ͭ0��^Yo��Ӹˎ����*w�7���\�/����R81��u��f���R�Jz-��R��KXHDҖz� ����8'w��
��4{g��uU�fk�{�c��,H胹7I�׈�b��M#4&�:=а	��/��)�bB�� ��o�W9�V�0�����a��d���W�   e       H      � �   Q      � �   Z      � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW  8                        4     LIBN      TLVSR      hRTSG      |CCST      �LIvi      �CONP      �TM80      �DFDS      �LIds      �VICD      vers     GCPR      �ICON      �icl8      �CPC2      �LIfp      �FPHb      �FPSE      �VPDP      LIbd       BDHb      4BDSE      HVITS      \DTHP      pMUID      �HIST      �VCTP      �FTAB      �    ����                                   ����       �        ����       �        ����       �        ����              ����               ����      H        ����      �        ����      �       ����             ����      ,       ����      <       	����      L       
����      \        ����      l        ����      �        ����              ����              ����              ����               ����              ����              ����               ����      0        ����      �        ����      �        ����      d        ����      l        ����      t        ����      �       �����      �    Channel name (Enum).ctl