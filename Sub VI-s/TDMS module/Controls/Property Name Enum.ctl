RSRC
 LVCCLBVW           �      Main_Class.lvclass    � �            < � @�      ����            �4+Yp�EM���L\          v���c�E��S��Sg���ُ ��	���B~       ���=�G��o�l�8   ����   �΃FMF�׊<{	ؑ�          H LVCC    VILB    PTH0   &      
Main_ClassMain_Class.lvclass             "   x�c`c`j`�� Č@������ �3  h6     H  x�c`��� H1200��,h�`Ʀ&�e..����P7�BD�bL{�&�T���) >�n�{j(    VIDS       q  �x�K`d`�4�0� ���X���!9?%���g��&�A T�����i@��/��7���vQh�Q�`*��Q��Qa�dQy������Gxw{�w���� �wQ� q�4���R3/ hGC�2C	�ၨ��L!Ì�B���'��h�@�5#����N� i�w=4�>����,ݍ@�މ (���!q܅CG�g<�	tN'��p���(Q�4��d2XM7�q�?D T���P j8��p�������v��9J��^����,��P�����b�� �@�HzX��d�@Ձ؇�l�]lP1^��	P��� eK��l% [ ���?@�zP����+r��7 >9q�       �$  15.0     �   15.0     �$  15.0     �   15.0     �$  15.0                       ������  �  �@ �� a�� ��  �� !�� A�� ��� �� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��              ��    ��      +����            �  �   ��   ��V                    �   ��       ���               �    ��       ���              �     ��       ���             �      ��       ���             ����   ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������          FPHP       �  �x��TOHQ�޸֛u�7��:���i׶	Oacd��&u���[���0B���t��A���X�̡��æ'�K)��雷;�jP�.|�����7 ��Z%�@�>��X���P�i��F�/ Mab�:�6%����XM����]�%�����'Xɂ64d��/2���/���
��E�H����.��P̄W�$q���@��]Og�{�r���lӭ�^<��(�NP�8�I�O	H�����*H)�4a㢇�#���Ө[�x�K`��:K���:�<��(��n1�"�?r���U���moo#k7iC/��0��|���a��p������+C^
�����6�Ϸl��X�Rf��
oD
'��0rE� ��nƐ�G:�П��>�7�(�d�8[`�ʟ�UĜ8���~�ԌB> ��;��f.ޝ�(���Э[�y^	V�X����c�|�Bw�NO�ґA#{77�0lL3�츁�������)#2��e�gQ��%!�1�B(����rX߬��ĩ��R�!l����WY���4���ٺ����X��j�M��"j��@|���;���Cnp�ϡ�^�d���tʗ��f�-Ju�q�>��M���x���KZ����@�E� ����(7t�[���g+�G�'64�?�D�� �g�x�t��ҽ���T� ͏�������li�                  BDHP        b   rx�c``��`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-�����z�\�8Se�<� b           �      !_ni_LastKnownOwningLVClassCluster  b �      0����     F   \    $@@ ����  Triangle Start indexes  @T Demod Signal  @@ ���� Triangle part @ Chirp Number  @
 Chirp ID  @
 Chirp Start @
 	Chirp end @
 Chirp Duration  @
 Freq. start @
 	Freq. end @
 	Deviation Q@
 JRMS deviation of chirp frequency from an ideal linear frequency trajectory  Q@
 JMax deviation of chirp frequency from an ideal linear frequency trajectory  U@
 NAverage deviation of chirp frequency from an ideal linear frequency trajectory  C@
 =Average chirp frequency (with respect to center of the chirp) %@
 mean Deviation for this section @
 Chirp Rate (Hz/us)  @
 CW Freq.  g �       Main_Class.lvclassAnalyzed signal cluster.ctl 0@P       	 
        Cluster  @@ ���� Analyzed CW Signal  &@@ ���� Analyzed Rising Sawtooth  &@@ ���� Analyzed Falling Sawtooth @@ ����  CW End  @@ ����  CW Start  @P   	CW Period @@ ����  
Fall Start  @@ ����  Fall End   @P   Falling Chirp Period  @T Demod signal  @@ ���� Falling Part  @@ ����  Rise End  @@ ����  
Rise Start  @P    Rising Chirp Period @@ ���� Rising part @@ ���� Analyzed Triangle> �       Main_Class.lvclassProcessing States.ctl@ TDMS config.Start DemodulationGet Demod. data IQ RateGet RAW data IQ RateReady For New RunStream. waitDemod. Read WaitDemod write. WaitProcess. WaitConfig StreamStreamFind OFF periods
Find BurstMeasurementsSpectrum
Spectogram  Processing States @
 RBW for Spectogram  @
 RBW of Spectrum @
 Spectogram End  @
 Spectogram Start  @
 Spectrum End  @
 Spectrum Start  @
 	Time Step @2����Data location S �       Main_Class.lvclassStream data duration.ctl @
 Stream data duration  
@!stop  @!status  @ code  @0����source  @P  / 0 1Error Cluster A �       Main_Class.lvclassStop to read.ctl @
 Stop o read F �       Main_Class.lvclassStart to read.ctl @
 Start for read  @p "	TDMS Path @
 Demod. signal IQ rate @
 RAW data IQ rate  @@ ����  OFF End @@ ����  	OFF Start @P  8 9
OFF Period  @T Demodulated Signal  @
 CW Deviation  @
 Numeric @@ ���� =Freq. start @@ ���� =	Freq. end @@ ���� =	Deviation  @@ ���� =Chirp Rate (Hz/us)  @@ ���� =Chirp Start @@ ���� =	Chirp end @@ ���� =Chirp Duration  @
 	rms value X@@ ���� EJRMS deviation of chirp frequency from an ideal linear frequency trajectory   
  X@@ ���� GJMax deviation of chirp frequency from an ideal linear frequency trajectory  @
 mean  \@@ ���� INAverage deviation of chirp frequency from an ideal linear frequency trajectory  J@@ ���� ==Average chirp frequency (with respect to center of the chirp) ,@@ ���� =mean Deviation for this section @ Numeric @@ ���� MChirp Number  @@ ���� =Chirp ID  @@ ���� =CW Freq.  0@P  > ? @ A B C D F H J K L N O P
Table data  @ Last chirp number  0���� $@@ �������� SMeasurement Table   @
 Carrier frequency @
 Power in band @
 Raw data power  @
 Span for power in band  @
 TDMS Read start @
 TDMS Read Stop  j@P (         ! " # $ % & ' ( ) * + , - . 2 3 4 5 6 7 : ; < Q R T U V W X Y ZMain_Class.lvclass   [       NI.LV.All.SourceOnly    �      !                  3   (                                        �  ~x��PKN�0��h)R�>A7��H|VE��J^
�I�ԭ`Ǒ�L�"���gFo�l x�7�L�k�t��f3w��ɳ���R��h1w嶘���:�A���[��r�_Ɖ�R�2;q��k'&�^�i�.�2@}A�ч�,�Q:w+�O]%�%��1�b�]����,�Ѓ���c`����	��eP��3W����'8%�����B��S�{�S��ܐ��j��2�?AE>�    e       H      � �   Q      � �   Z      � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW           �               4     LIBN      TLVSR      hRTSG      |CCST      �LIvi      �CONP      �TM80      �DFDS      �LIds      �VICD      vers     GCPR      �ICON      �icl8      �CPC2      �LIfp      �FPHb      �FPSE      �VPDP      LIbd       BDHb      4BDSE      HVITS      \DTHP      pMUID      �HIST      �VCTP      �FTAB      �    ����                                   ����       �        ����       �        ����       �        ����              ����               ����      H        ����      �        ����      �       ����             ����      ,       ����      <       	����      L       
����      \        ����      l        ����      �        ����              ����              ����              ����               ����      
�        ����      
�        ����               ����              ����      x        ����      �        ����      D        ����      L        ����      T        ����      �       �����      t    Property Name Enum.ctl